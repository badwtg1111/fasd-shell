;;; fasd-shell.el --- Use the fasd command line utility to cd to recently visited directories (and more), with ido completion.


;; Author: vindarel
;; Package-Requires: (s.el)
;; URL:
;; Keywords: cli shell-mode bash zsh autojump

;;; Commentary:

;; In shell mode, make the TAB key to trigger fasd with ido completion.
;; Type "d  lisp RET" to have ido asking what directory matching "lisp" to cd to.
;; todo: make other options than "d" to change directory.

;;; Requirements:

;; `fasd' command line tool, see: https://github.com/clvv/fasd

;;; Usage:

;; (require 'fasd-shell)
;; (fasd-shell-mode t)

;;; Code:

(require 's)

(defgroup fasd-shell nil
  "Quickly cd to previously-visited directories in shell mode, with ido-completion."
  :group 'toolss
  :group 'convenience)

(defun fasd-get-path-list (pattern)
  "call fasd with pattern and return the list of possibilities"
  (s-split "\n" (s-trim (shell-command-to-string (format "fasd -l -R %s" pattern))))
)

(defun fasd ()
  "If current shell command is `d something' call fasd"
  (interactive)
  (let* ((user-input (buffer-substring-no-properties (comint-line-beginning-position)
                                                     (point-max))))
    (if (and (string= (substring user-input 0 2) "d "))  ;; todo: mapping to use something else than d and change directory.
        (progn
          ;; get what is after "d "
          (setq fasd-pattern (buffer-substring-no-properties (+ (comint-line-beginning-position) 2) (point-max)))
          (setq fasd-command (concat "cd " (ido-completing-read "cd to: " (fasd-get-path-list fasd-pattern))))
          (comint-kill-input)
          (insert fasd-command)
          (comint-send-input)
          ))
    ;; trigger the normal TAB completion:
    ;; (yas-expand)
    (completion-at-point)
    ))


(define-minor-mode fasd-shell-mode
  "Toggle fasd completion in shell mode.
   "
  :global t
  :group 'fasd-shell

  (if fasd-shell-mode
      ;; Use TAB as in normal shell. Now we have even better completion than in zsh !
      (progn (define-key shell-mode-map (kbd "<tab>") 'fasd)
             )
    (define-key shell-mode-map (kbd "<tab>") 'completion-at-point)
))

(provide 'fasd-shell)
;;; fasd-shell ends here
